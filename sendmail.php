<?php

$name     = $_POST["name"];
$mail     = $_POST["mail"];
$text     = $_POST["content"];
$token    = $_POST["token"];
$gCaptcha = $_POST['g-recaptcha-response'];

if ( !empty($name) && !empty($mail) && !empty($text) && empty($token) ) {
    $ch = curl_init("https://www.google.com/recaptcha/api/siteverify");

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "secret=6LfbMBIUAAAAACX-w-61vEgD5huywlCWyoA7hsES&response=" . $gCaptcha);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $response = json_decode(curl_exec($ch), true);

    if ( $response["success"] ) {
        mb_internal_encoding("UTF-8");
        $header = 'From: me@zigastrgar.com';
        $header .= "\nMIME-Version: 1.0\n";
        $header .= "Content-Type: text/html; charset=\"utf-8\"\n";
        $address = "ziga_strgar@hotmail.com";
        $subject = "Sporočilo od: " . $name;
        $message = $text . "<br /><br/>" . $mail;
        if ( mb_send_mail($address, $subject, $message, $header) ) {
            echo "success|Hvala Vam da ste me kontaktirali, odgovorim takoj ko bom lahko ;)";
        } else {
            echo "error|Napaka pri pošiljanju!";
        }
    } else {
        echo "error|Napaka v recaptcha";
    }
} else {
    echo "error|Napaka podatkov, prosim izpolnite vsa polja!";
}